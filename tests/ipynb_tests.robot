*** Settings ***
Documentation     A test suite with a single test.
 
Resource          ../resources/resource.robot

*** Test Cases ***
Add and Run Code In ipynb File
    [Tags]    Smoke
    Open Browser To App Start Page
    Click New Launcher Button
    Click New Notebook Button    ${PYTHON3}  
    Check Elemenet is Shownin The Left Panel    ${PYTHONNOTEBOOK}  
    Double Click On File In the Left Panel    ${PYTHONNOTEBOOK} 
    #todo
    #add new row
    #add some python code
    #execute python code
    #check res
    [Teardown]    Close Browser