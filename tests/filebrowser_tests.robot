*** Settings ***
Documentation     A test suite containing tests related to folder tree.

Suite Setup       Open Browser To App Start Page
Suite Teardown    Close Browser
Test Setup        Left Panel Is Expand
Test Template     Folders Are Shown
Resource          ../resources/resource.robot

*** Test Cases ***              FOLDER NAME
Notebooks folder is shown       ${NOTEBOOKS}
    [Tags]    Smoke
Data folder is shown            ${DATA}
    [Tags]    Smoke
TCGA_Data folder is shown       ${TCGADATA}

*** Keywords ***
Folders Are Shown
    [Arguments]    ${foldername}
    Check Elemenet is Shownin The Left Panel    ${foldername}