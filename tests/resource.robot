*** Settings ***
Documentation     A resource file with reusable keywords and variables.

Library           SeleniumLibrary

*** Variables ***
${STARTLINK}                     https://mybinder.org/v2/gh/jupyterlab/jupyterlab-demo/master?urlpath=lab/tree/demo
${BROWSER}                       Firefox 
${DELAY}                         0
${NOTEBOOKS}                     notebooks
${DATA}                          data
${TCGADATA}                      TCGA_Data
${PYTHON3}                       Python 3
${CPP11}                         C++11
${CPP14}                         C++14
${CPP17}                         C++17
${R}                             R                           

#XPath
${ICONHOMEPAGEXPATH}             //*[@data-icon='ui-components:jupyter']
${ICONNOTEBOOKSFOLDERXPATH}      //span[contains(@class, 'jp-DirListing-itemText') and text() = '${NOTEBOOKS}']
${FOLDERSLISTXPATH}              //ul[@class='jp-DirListing-content']
${COLLAPSELEFTPANELXPATH}        //div[@class='lm-Widget p-Widget jp-FileBrowser lm-StackedPanel-child p-StackedPanel-child lm-mod-hidden p-mod-hidden']
${ACTIVELEFTPANELXPATH}          //div[@class='lm-Widget p-Widget jp-FileBrowser lm-StackedPanel-child p-StackedPanel-child']
${FOLDERBUTTONXPATH}             //li[@class='lm-TabBar-tab p-TabBar-tab lm-mod-current p-mod-current']
${FOLDERBUTTONXCOLLAPSEPATH}     //li[@class='lm-TabBar-tab p-TabBar-tab'][1]
${ADDNEWLAUNCHERBUTTONXPATH}     //button[contains(@title, 'New Launcher')]
${ADDNOTEBOOKXPATH}              //div[contains(@title, '${PYTHON3}') and contains(@data-category, 'Notebook')]
#todo!!!!!
${NEWLAUNCHERXPATH}              //*[@id='jp-main-dock-panel']/div[4]/ul/li[2]/div[2] 
${ROWXPATH}                      
#to do auto counter
${PYTHONNOTEBOOK}                Untitled.ipynb
${CPP11NOTEBOOK}                 Untitled1.ipynb
${CPP14NOTEBOOK}                 Untitled2.ipynb
${CPP17NOTEBOOK}                 Untitled3.ipynb
${RNOTEBOOK}                     Untitled4.ipynb

*** Keywords ***
Open Browser To App Start Page
     Open Browser    ${STARTLINK}    ${BROWSER}
     Maximize Browser Window
     Set Selenium Speed    ${DELAY}
     Home Page Should Be Open

Home Page Should Be Open
    Title Should Be    Binder
    Wait until Page Contains Element    ${ICONHOMEPAGEXPATH}    120s
    Title Should Be    JupyterLab
    Wait until Page Contains Element    ${ICONNOTEBOOKSFOLDERXPATH}    15S

#Check elements
Left Panel Is Expand
    Element Should Be Visible     ${ACTIVELEFTPANELXPATH}

Check Elemenet is Shownin The Left Panel
    [Arguments]    ${foldername}
    Wait until Page Contains Element    //span[contains(@class, 'jp-DirListing-itemText') and text() = '${foldername}']

Left Panel Is Collapse
    Element Should Not Be Visible     ${COLLAPSELEFTPANELXPATH}
    
Launch is Shown
    Element Should Be Visible     ${NEWLAUNCHERXPATH}  

#Clicks Steps
Click Folder Button To Collapse Left Panel
    Click Element      ${FOLDERBUTTONXPATH}

Click Folder Button To Show Left Panel
    Click Element      ${FOLDERBUTTONXCOLLAPSEPATH}

Click New Launcher Button
    Click Button    ${ADDNEWLAUNCHERBUTTONXPATH}  

Click New Notebook Button
    [Arguments]    ${filetype}
    Click Element    //div[contains(@title, '${filetype}') and contains(@data-category, 'Notebook')]

Double Click On File In the Left Panel
    [Arguments]    ${foldername}
    Double Click Element    //span[contains(@class, 'jp-DirListing-itemText') and text() = '${foldername}']
