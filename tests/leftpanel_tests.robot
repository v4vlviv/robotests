*** Settings ***
Documentation     A test suite with a single test.
 
Resource          ../resources/resource.robot

*** Test Cases ***
Validate Left Menu Panel
    Open Browser To App Start Page
    Left Panel Is Expand
    Click Folder Button To Collapse Left Panel
    Left Panel Is Collapse
    Click Folder Button To Show Left Panel
    Left Panel Is Expand
    
    [Teardown]    Close Browser


