*** Settings ***
Documentation     A test suite containing tests related to .
 
Suite Setup       Open Browser To App Start Page
Suite Teardown    Close Browser
Test Setup        Left Panel Is Expand
Test Template     Add New File
Resource          ../resources/resource.robot

*** Test Cases ***               FILE TYPE                FILE NAME
Python Notebook Added      ${PYTHON3}                  ${PYTHONNOTEBOOK}
    [Tags]    Smoke
CPP11 Notebook Added       ${CPP11}                    ${CPP11NOTEBOOK} 
CPP14 Notebook Added       ${CPP14}                    ${CPP14NOTEBOOK}
CPP17 Notebook Added       ${CPP17}                    ${CPP17NOTEBOOK}
R Notebook Added           ${R}                        ${RNOTEBOOK} 


*** Keywords ***
Add New File
    [Arguments]    ${filetype}    ${filename}
    Left Panel Is Expand
    Click New Launcher Button
    Launch is Shown
    Click New Notebook Button    ${filetype} 
    Check Elemenet is Shownin The Left Panel    ${filename} 