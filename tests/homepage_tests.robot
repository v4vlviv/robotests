*** Settings ***
Documentation     A test suite with a single test for Home Page.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../resources/resource.robot

*** Test Cases ***
Open Home Page
    Open Browser To App Start Page
    Left Panel Is Expand
    [Teardown]    Close Browser
