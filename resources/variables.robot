*** Settings ***
Documentation     A resource file with reusable keywords and variables.

Library           SeleniumLibrary

*** Variables ***
#robot -v ENV:qa -i Smoke .
&{URL}                           qa=https://mybinder.org/v2/gh/jupyterlab/jupyterlab-demo/master?urlpath=lab/tree/demo    dev=dev.com    prod=prod.com
${ENV}                           qa
&{BROWSER}                       chrome=Chrome     ff=FF
${DEFAULT}               chrome
${DELAY}                         0
${NOTEBOOKS}                     notebooks
${DATA}                          data
${TCGADATA}                      TCGA_Data
${PYTHON3}                       Python 3
${CPP11}                         C++11
${CPP14}                         C++14
${CPP17}                         C++17
${R}                             R
${TMP_PATH}                      /tmp

#XPath
${ICONHOMEPAGEXPATH}             //*[@data-icon='ui-components:jupyter']
${ICONNOTEBOOKSFOLDERXPATH}      //span[contains(@class, 'jp-DirListing-itemText') and text() = '${NOTEBOOKS}']
${FOLDERSLISTXPATH}              //ul[@class='jp-DirListing-content']
${COLLAPSELEFTPANELXPATH}        //div[@class='lm-Widget p-Widget jp-FileBrowser lm-StackedPanel-child p-StackedPanel-child lm-mod-hidden p-mod-hidden']
${ACTIVELEFTPANELXPATH}          //div[@class='lm-Widget p-Widget jp-FileBrowser lm-StackedPanel-child p-StackedPanel-child']
${FOLDERBUTTONXPATH}             //li[@class='lm-TabBar-tab p-TabBar-tab lm-mod-current p-mod-current']
${ADDNEWLAUNCHERBUTTONXPATH}     //button[contains(@title, 'New Launcher')]
${ADDNOTEBOOKXPATH}              //div[contains(@title, '${PYTHON3}') and contains(@data-category, 'Notebook')]
${ADDNEWTERMINALBUTTONXPATH}     //div[@title='Start a new terminal session']/div[@class='jp-LauncherCard-icon']
#todo!!!!!
${NEWLAUNCHERXPATH}              //*[@id='jp-main-dock-panel']/div[4]/ul/li[2]/div[2] 
${FOLDERBUTTONXCOLLAPSEPATH}     //li[@class='lm-TabBar-tab p-TabBar-tab'][1]
${ROWXPATH}                      
#to do auto counter
${PYTHONNOTEBOOK}                Untitled.ipynb
${CPP11NOTEBOOK}                 Untitled1.ipynb
${CPP14NOTEBOOK}                 Untitled2.ipynb
${CPP17NOTEBOOK}                 Untitled3.ipynb
${RNOTEBOOK}                     Untitled4.ipynb