*** Settings ***
Documentation     A resource file with reusable keywords and variables.

Library           SeleniumLibrary
Library           XvfbRobot
Resource          ../resources/variables.robot

*** Keywords ***
Open Browser To App Start Page
#    Start Virtual Display    1920    1080
    Run Keyword If  '${BROWSER.${DEFAULT}}' == 'FF'  Open FF Browser    ELSE IF    '${BROWSER.${DEFAULT}}' == 'Chrome'  Open Chrome Browser
    GoTo    ${URL.${ENV}}
    Maximize Browser Window
    Home Page Should Be Open

Open Chrome Browser
    ${options}  Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys, selenium.webdriver
    Call Method    ${options}    add_argument    --no-sandbox
    Call Method    ${options}    add_argument    --headless
    Call Method    ${options}    add_argument    --disable-extensions
    Call Method    ${options}    add_argument    --disable-gpu
    Call Method    ${options}    add_argument    --disable-dev-shm-usage
    ${prefs}    Create Dictionary    download.default_directory=${TMP_PATH}
    Call Method    ${options}    add_experimental_option    prefs    ${prefs}
    Create Webdriver    Chrome    chrome_options=${options}

Open FF Browser
    Open Browser      ${URL.${ENV}}   headlessfirefox

Home Page Should Be Open
    Title Should Be    Binder
    Wait until Page Contains Element    ${ICONHOMEPAGEXPATH}    120s
    Title Should Be    JupyterLab
    Wait until Page Contains Element    ${ICONNOTEBOOKSFOLDERXPATH}    15S

#Check elements
Left Panel Is Expand
    Element Should Be Visible     ${ACTIVELEFTPANELXPATH}

Check Elemenet is Shownin The Left Panel
    [Arguments]    ${foldername}
    Wait until Page Contains Element    //span[contains(@class, 'jp-DirListing-itemText') and text() = '${foldername}']

Left Panel Is Collapse
    Element Should Not Be Visible     ${COLLAPSELEFTPANELXPATH}
    
Launch is Shown
    Element Should Be Visible     ${NEWLAUNCHERXPATH}  

#Clicks Steps
Click Folder Button To Collapse Left Panel
    Click Element      ${FOLDERBUTTONXPATH}

Click Folder Button To Show Left Panel
    Click Element      ${FOLDERBUTTONXCOLLAPSEPATH}
    
Click New Terminal Button
    Click Element    ${ADDNEWTERMINALBUTTONXPATH}

Click New Launcher Button
    Click Button    ${ADDNEWLAUNCHERBUTTONXPATH}  

Click New Notebook Button
    [Arguments]    ${filetype}
    Click Element    //div[contains(@title, '${filetype}') and contains(@data-category, 'Notebook')]

Double Click On File In the Left Panel
    [Arguments]    ${foldername}
    Double Click Element    //span[contains(@class, 'jp-DirListing-itemText') and text() = '${foldername}']
