#!/usr/bin/env bash

docker run --rm \
           -v "$PWD/output":/output \
           -v "$PWD/tests":/tests \
           -v "$PWD/resources":/resources \
           -v "$PWD/scripts":/scripts \
           -v "$PWD/reports":/reports \
           --security-opt seccomp:unconfined \
           --shm-size "512M" \
           kilina/robot-framework-selenium \
           /scripts/run_reg_parallel.sh